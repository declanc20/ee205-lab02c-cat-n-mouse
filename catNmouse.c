///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
///
/// This is a classic "I'm thinking of a number" guessing game.  The mouse
/// will think of a number... and the cat will keep trying to guess it.
///
/// @file    catNmouse.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o catNmouse catNmouse.c
///
/// Usage:  catNmouse [n]
///   n:  The maximum number used in the guessing game
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// Example:
///   $ ./catNmouse 2000
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1385
///   No cat... the number I’m thinking of is smaller than 1385
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1382
///   No cat... the number I’m thinking of is larger than 1382
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1384
///   You got me.
///   |\---/|
///   | o_o |
///    \_^_/
///   
/// @author  Declan Campbell < declanc@hawaii.edu>
/// @date     01_21_2022
///////////////////////////////////////////////////////////////////////////////

#define DEFAULT_MAX_NUMBER 2048

#include <stdio.h>
#include<stdlib.h>
#include<time.h>

int main( int argc, char* argv[] ) {
   

int setmaxnum = DEFAULT_MAX_NUMBER; //initialzie max number to default
  
/*figure out if user set a max number*/
   if (argc > 1){
   setmaxnum = atoi(argv[1]); //set the max number
      if (setmaxnum <1){ //if input is not greater than or equal to one print error
      printf("error, invalid number\n");
      return 1; //exit with status of 1
      }
   }

srand(time(0)); //need to seed rand function so cat is not thinking of the same number each time
   int theNumberImThinkingOf = (rand() % (setmaxnum))+1; //generates the number the cat is thinking of 
   int aGuess; //declare users guess input variable




   do { //do while loop starts here 
   printf( "OK cat, I'm thinking of a number from 1 to %d. Make a guess: \n", setmaxnum ); //print the prommpt to player
   scanf( "%d", &aGuess ); //take the players guess

   while (aGuess < 1 || aGuess > setmaxnum){ //keep asking for input if needed until valid input is given
      printf("make sure your guess is greater than 0 and less than %d \n", setmaxnum);
      scanf( "%d", &aGuess );
   }
  
   if (aGuess == theNumberImThinkingOf){ //print out fun cat art if they get it :)
     printf("You got me\n");
     printf(" /\\_/\\  ( \n");
     printf("( ^.^ ) _) \n");
     printf("  \\\"/  (\n");
     printf(" ( | | )\n");
     printf("(__d b__)\n");
   }

   else { //if number guessed was not what i was thinking do this
      if (aGuess > theNumberImThinkingOf){ // if guess was smaller than number thinking if print this
         printf("No cat... the number I’m thinking of is smaller than %d\n", aGuess);
      }

      else if (aGuess < theNumberImThinkingOf){ //if number guessed was larger than what im thinking of print this
         printf("No cat... the number I’m thinking of is larger than %d\n", aGuess);
      }
   
      
   }
   } while (aGuess != theNumberImThinkingOf);
   //end while loop here

   //printf( "The number was [%d]\n", aGuess ); //showed that scanf was working

   return 0;  // exit program with status of zero
}

